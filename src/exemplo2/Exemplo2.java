/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemplo2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author bruno
 */
public class Exemplo2 {
    public static String  ganhador;
    
    public static void main(String [] args) throws InterruptedException{
        System.out.println("Inicio da criacao das threads.");
        
        ExecutorService threadPool = Executors.newCachedThreadPool();
        
        int menorTempo = 9999999;
        
        for (int i = 1; i <= 4; i++) {
            //Cria cada thread com um novo runnable selecionado
            PrintTasks carro = new PrintTasks("Carro" + i);
            
            Thread t = new Thread(carro);
            //Inicia as threads, e as coloca no estado EXECUTAVEL
            threadPool.execute(t);
            
            while(!carro.isAcabou()){
                Thread.sleep(1);
            }
            
            if (menorTempo > carro.getTotalTime()){
                menorTempo = carro.getTotalTime();
                ganhador = carro.getTaskName();
            }
        }    
        threadPool.shutdown();
        
        System.out.println("Threads criadas");
        System.out.println("ganhador: " + ganhador + " - tempo: " + menorTempo + " ms");
        
    }
    
}
