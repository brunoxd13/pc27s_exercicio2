/**
 * Exemplo2: Programacao com threads
 * Autor: Bruno Russi Lautenschlager
 * Ultima modificacao: 07/08/2017
 */
package exemplo2;

import java.util.Random;

/**
 *
 * @author Bruno Russi Lautenschlager
 */
public class PrintTasks implements Runnable {
    private int sleepTime; //tempo de adormecimento aleatorio para a thread
    private final String taskName; //nome da tarefa
    private final Random generator = new Random();
    private static int _ID;
    private int totalTime;
    private boolean acabou = false;
    
    public PrintTasks(String name){
        taskName = name;
        
        _ID++;
        System.out.printf("Id:" + _ID + "\n");
    }
    
    public void run(){
        try{
            for (int i = 0; i < 5; i++) {
                sleepTime = generator.nextInt(1000); //milissegundos
        
                System.out.printf("Tarefa: %s dorme por %d ms\n", getTaskName(), sleepTime);
                Thread.sleep(sleepTime);

                totalTime += sleepTime;
            }
            
        } catch (InterruptedException ex){
            System.out.printf("%s %s\n", getTaskName(), "terminou de maneira inesperada.");
        }
       System.out.printf("%s acordou!\n", getTaskName());
       
       this.acabou = true;
    }

    /**
     * @return the totalTime
     */
    public int getTotalTime() {
        return totalTime;
    }

    /**
     * @return the taskName
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * @return the acabou
     */
    public boolean isAcabou() {
        return acabou;
    }

}
